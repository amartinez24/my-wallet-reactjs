# Mi billetera virtual
## _Lleva el control de todos tus gastos_

powered by  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Npm-logo.svg/245px-Npm-logo.svg.png" width="52" height="28" alt="npm-icon"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/245px-React.svg.png" width="32" height="28" alt="npm-icon"/>

Current version: alpha


To see the current preview: https://amartinez24.gitlab.io/my-wallet-reactjs
