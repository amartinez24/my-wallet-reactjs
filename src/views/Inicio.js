import React, {Component} from 'react';
import ReactApexChart from  'react-apexcharts';

import '../assets/css/views/Inicio.css';

class Inicio extends Component {
    render() {
        const options = {
            series: [70, 30],
            labels: ['Ingresos', 'Gastos'],
            chart: {
                type: 'donut',
                width: 360 },
            legend: {
                show: true,
                position: 'bottom' }
        };
        
        return (
            <React.Fragment>
                <div className="Inicio">
                    <h2>Inicio</h2>
                    <ReactApexChart className="chart0" options={options} series={options.series} type={options.chart.type} width={options.chart.width} />
                </div>
            </React.Fragment>
        );
    }
}

export default Inicio;
