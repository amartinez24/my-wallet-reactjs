import React, {Component} from 'react';
import {withRouter}  from 'react-router-dom';
import logoApp from '../assets/images/billetera_k-48.png';
import iconAccount from '../assets/images/iconAccount.svg';
import '../assets/css/Nav.css';

class Nav extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="Nav fixed-top navbar navbar-dark bg-dark container-fluid">

                    <div className="col-5 row justify-content-center mr-5">
                        <button className="navbar-toggler Nav-tool-btn" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon Nav-tool-btn-icon"></span>
                        </button>
                        <span className="Nav-tool-btn-name">Herramientas</span>
                    </div>

                    <div className="col">
                        <img className="Nav-app-icon rounded img-fluid" src={logoApp} alt="logo"/>
                        <label className="Nav-app-title">Mi billetera virtual</label>
                    </div>

                    <div className="col-3">
                        <button className="Nav-account-btn dropdown navbar-toggler dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">   
                            <img className="Nav-account-btn-image rounded img-fluid" src={iconAccount} alt="Foto de perfil"/>
                            <span className="Nav-account-name">Iniciar sesión</span>
                        </button>
                        <div className="dropdown-menu dropdown-menu-position collapse row" aria-labelledby="navbarDropdown">
                            <a className="dropdown-item collapse" href="#account">Mi cuenta</a>
                            <a className="dropdown-item" href="#login">Iniciar sesión</a>
                            <a className="dropdown-item collapse" href="#logout">Desloguearse</a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#signup">Registrarse</a>
                            <a className="dropdown-item" href="#help">Ayuda</a>
                            <a className="dropdown-item" href="#privacy">Política de privacidad</a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#about">Acerca de...</a>
                        </div>
                    </div>
                </div>

                <div className="Nav-tools navbar-collapse collapse bg-dark container-fluid" id="navbarToggleExternalContent">
                    <form className="form-inline mt-2">
                        <input className="form-control ml-auto" type="search" placeholder="¿Qué desea buscar?" />
                        <button className="btn btn-outline-success mr-auto ml-1" type="submit">Buscar</button>
                    </form>
                </div>
            </React.Fragment>
        )
    }
};

Nav = withRouter(Nav);
export default Nav;
