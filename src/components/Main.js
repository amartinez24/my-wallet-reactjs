import React, {Component} from 'react';
import {withRouter}  from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';

import '../assets/css/Main.css';

import Inicio from '../views/Inicio';
import Ingresos from '../views/Ingresos';
import Gastos from '../views/Gastos';
import Planificacion from '../views/Planificacion';
import Configuracion from '../views/Configuracion';

class Main extends Component {
    render() {
        return (
            <React.Fragment>
                <div class="Main tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-inicio" role="tabpanel" aria-labelledby="list-inicio-list">
                        <BrowserRouter>
                            <Inicio />
                        </BrowserRouter>
                    </div>
                    <div class="tab-pane fade" id="list-ingresos" role="tabpanel" aria-labelledby="list-ingresos-list">
                        <BrowserRouter>
                            <Ingresos />
                        </BrowserRouter>
                    </div>
                    <div class="tab-pane fade" id="list-gastos" role="tabpanel" aria-labelledby="list-gastos-list">
                        <BrowserRouter>
                            <Gastos />
                        </BrowserRouter>
                    </div>
                    <div class="tab-pane fade" id="list-planificacion" role="tabpanel" aria-labelledby="list-planificacion-list">
                        <BrowserRouter>
                            <Planificacion />
                        </BrowserRouter>
                    </div>
                    <div class="tab-pane fade" id="list-configuracion" role="tabpanel" aria-labelledby="list-configuracion-list">
                        <BrowserRouter>
                            <Configuracion />
                        </BrowserRouter>
                    </div>
                </div>
            </React.Fragment>
        )
    }
};

Main = withRouter(Main);
export default Main;
