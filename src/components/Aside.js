import React, {Component} from 'react';
import {withRouter}  from 'react-router-dom';
import '../assets/css/Aside.css';

class Aside extends Component {
    render() {
        return (
            <div className="Aside">
                <div className="Aside-content list-group" id="list-tab" role="tablist">
                    <a className="btn3d-Header list-group list-group-item disabled noselect">
                        <span className="btn3d-Header-text">Panel de navegación</span>
                    </a>
                    <a className="Aside-1 btn3d list-group list-group-item list-group-item1 list-group-item-action active"
                     id="list-inicio-list" data-toggle="list" href="#list-inicio" role="tab" aria-controls="inicio">
                        Inicio
                    </a>
                    <a className="Aside-2 btn3d list-group list-group-item list-group-item2 list-group-item-action"
                     id="list-ingresos-list" data-toggle="list" href="#list-ingresos" role="tab" aria-controls="ingresos">
                        Ingresos
                    </a>
                    <a className="Aside-3 btn3d list-group list-group-item list-group-item3 list-group-item-action"
                     id="list-gastos-list" data-toggle="list" href="#list-gastos" role="tab" aria-controls="gastos">
                        Gastos
                    </a>
                    <a className="Aside-4 btn3d list-group list-group-item list-group-item4 list-group-item-action"
                     id="list-planificacion-list" data-toggle="list" href="#list-planificacion" role="tab" aria-controls="planificacion">
                        Planificaciones
                    </a>
                    <a className="Aside-5 btn3d list-group list-group-item list-group-item5 list-group-item-action"
                     id="list-configuracion-list" data-toggle="list" href="#list-configuracion" role="tab" aria-controls="configuracion">
                        Configuración
                    </a>
                </div>
            </div>
        )
    }
};

Aside = withRouter(Aside);
export default Aside;