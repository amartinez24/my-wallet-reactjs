import React, {Component} from 'react';
import {withRouter}  from 'react-router-dom';
import logoReact from '../assets/images/logoReact.svg';
import '../assets/css/Header.css';

class Header extends Component {
    render() {
        return (
            <header className="Header fixed-top">
                    <img className="Header-logo" src={logoReact} alt="ReactJS"/>
            </header>
        )
    }
};

Header = withRouter(Header);
export default Header;