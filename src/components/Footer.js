import React, {Component} from 'react';
import {withRouter}  from 'react-router-dom';
import '../assets/css/Footer.css';

class Footer extends Component {
    render() {
        return (
            <footer className="Footer bg-dark fixed-bottom container-fluid">          
                <label className="Footer-text-left">
                Aplicación creada con ReactJS -
                </label>
                <a id="githubLink" href="https://github.com/alanjmrt94" target="new"> @alanjmrt94 </a>
                <label className="Footer-text-right">
                    - Abr, 2021
                </label> 
            </footer>
        )
    }
};

Footer = withRouter(Footer);
export default Footer;