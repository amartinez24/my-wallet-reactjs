import '../assets/css/App.css';

import {BrowserRouter} from 'react-router-dom';

import Header from './Header';
import Nav from './Nav';
import Aside from './Aside';
import Main from './Main';
import Footer from './Footer';

function App() {
  return (
    <div className="App">

      <BrowserRouter>
        <Header />
      </BrowserRouter>

      <BrowserRouter>
        <Nav />
      </BrowserRouter>

      <BrowserRouter>
        <Aside />
      </BrowserRouter>

      <BrowserRouter>
        <Main />
      </BrowserRouter>

      <BrowserRouter>
        <Footer />
      </BrowserRouter>

    </div>
  );
}

export default App;
